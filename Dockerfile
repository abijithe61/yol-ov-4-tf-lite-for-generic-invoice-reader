FROM ubuntu:18.04

WORKDIR /workspace

RUN apt-get update && apt-get install -y \
        software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update && apt-get install -y \
    python3.7 \
    python3-pip
RUN python3.7 -m pip install pip
RUN apt-get update && apt-get install -y \
    python3-distutils \
    python3-setuptools \
    git
RUN python3.7 -m pip install pip --upgrade pip

RUN useradd -s /bin/bash user

RUN apt-get -y install tesseract-ocr

#RUN git clone https://gitlab.com/abijithe61/yol-ov-4-tf-lite-for-generic-invoice-reader.git .

COPY requirements.txt ./

RUN apt-get install -y libsm6 libxext6 libxrender-dev

RUN pip install -r requirements.txt

COPY models/checkpoints ./models/checkpoints
COPY core ./core
COPY data ./data
COPY detections ./detections
COPY templates ./templates
COPY detect.py ./
COPY main.py ./

EXPOSE 7860

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "7860"]
